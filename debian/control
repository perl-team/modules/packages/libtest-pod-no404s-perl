Source: libtest-pod-no404s-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13), libmodule-build-tiny-perl
Build-Depends-Indep: perl,
                     libtest-pod-perl,
                     liburi-find-perl,
                     libwww-perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-pod-no404s-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-pod-no404s-perl.git
Homepage: https://metacpan.org/release/Test-Pod-No404s

Package: libtest-pod-no404s-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libtest-pod-perl,
         liburi-find-perl,
         libwww-perl
Description: test utility checking POD for HTTP 404 links
 Test::Pod::No404s looks for any http(s) links in your POD and verifies that
 they will not return a 404. It uses LWP::UserAgent for the heavy lifting, and
 simply lets you know if it failed to retrieve the document. More
 specifically, it uses $response->is_error as the "test."
 .
 Normally, you wouldn't want this test to be run during end-user installation
 because they might have no internet! It is HIGHLY recommended that this be
 used only for module authors' RELEASE_TESTING phase. To do that, just modify
 the synopsis to add an env check :)
